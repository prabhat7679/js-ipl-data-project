// Find a player who has won the highest number of Player of the Match awards for each season
const express = require('express')
const router = express.Router();

const path=require("path");
const jsonFileOutputPath=path.resolve(__dirname,'../public/output/6-highest-no-of-player-of-the-match-each-season.json');
const csvFilePathDeliveries=path.resolve(__dirname,'../data/deliveries.csv');
const csvFilePathMatches=path.resolve(__dirname,'../data/matches.csv');
const csv= require('csvtojson');
const fs = require('fs');

router.get('/api/palyerOfMatch/season',(req, res, next)=>{

   csv()
  .fromFile(csvFilePathMatches)
  .then((myMatchObj)=>{

     if(myMatchObj==undefined)
     {
      return {};
     }
    //    calculate season 
  
    const year= myMatchObj.reduce((year, Team)=>{
  
      if(year[Team.season]=== undefined)
      {
          year[Team.season]= {};
      }
  
        if(year[Team.season][Team.player_of_match])
        {
          year[Team.season][Team.player_of_match]+=1;
        }else
        {
          year[Team.season][Team.player_of_match] =1;
        }
  
        return year;
     },{});
  //   console.log(year)
  
       const heightPlayerOfMatchEachSeason= Object.entries(year).reduce((player1, player2)=>{
  
            const keyAndValues = Object.entries(player2[1])
            .sort((number1,number2)=>{
              return number2[1] -number1[1];
            })
            .slice(0,1)  
            
            player1[player2[0]] = Object.fromEntries(keyAndValues);
            return player1;
  
          // console.log(keyAndValues)
  
       },{})
        
            if(heightPlayerOfMatchEachSeason === undefined)
            {
               res.next({
               message : 'Internal Server Error',
               status : 500
            })
            }else
            {
                res.status(200).json(heightPlayerOfMatchEachSeason);
            }

       //fs.writeFileSync(jsonFileOutputPath,JSON.stringify(heightPlayerOfMatchEachSeason,null,2));
    });

})

module.exports= router;
