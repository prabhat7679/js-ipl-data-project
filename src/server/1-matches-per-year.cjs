// Number of matches played per year for all the years in IPL.

const express = require('express')
const router = express.Router()

const path= require("path")
const csvFilePath=path.resolve(__dirname,"../data/matches.csv");

//   const jsonFileOutputPath=path.resolve(__dirname,"../public/output/1-matches-per-year.json");
const csv=require('csvtojson')

const fs=require('fs')

//const port = require('./config')

router.get('/api/matches-per-year',(req, res, next)=>{

   csv()
  .fromFile(csvFilePath)
  .then((myMatchObj)=>{

     if(myMatchObj=== undefined)
     {
        return {};
     }

          //   using Higher Order Function 
      const objectOfYear= myMatchObj.reduce((match, year)=> {
         if(match[year.season]=== undefined)
         {
            match[year.season]=1;
         }
         else
         {
            match[year.season]+=1;
         }

          return match;
       },{});  


       if(objectOfYear === undefined)
       {
         next(
            {
               message : 'Invalid',
               status : 500
            }
         )
       }else
       {
         res.status(200).json(objectOfYear)
       }

     // console.log(objectOfYear)
      // fs.writeFileSync(jsonFileOutputPath, JSON.stringify(objectOfYear,null,2),"utf-8",(err)=>{
      //    if(err) 
      //    {
      //       console.log(err);
      //    } 
      //  })
    }).catch((err)=>{

        next(err)
     })      
})

module.exports = router;

