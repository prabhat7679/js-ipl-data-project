//    Extra runs conceded per team in the year 2016
const express = require('express')
const router = express.Router();

const path=require("path");
const jsonFileOutputPath=path.resolve(__dirname,'../public/output/3-Extra-runs-conceded-per-team-in-the-year-2016.json');
const csvFilePathDeliveries=path.resolve(__dirname,'../data/deliveries.csv');
const csvFilePathMatches=path.resolve(__dirname,'../data/matches.csv');
const csv= require('csvtojson');
const fs = require('fs');

router.get('/api/extraRun/2016',(req, res, next)=>{
   
    csv()
   .fromFile(csvFilePathMatches)
   .then((myMatchObj)=>{
    
    if(myMatchObj==undefined)
    {
       return {};
    }
     // filter 2016 and 2016Id from given matches data
      const season2016Id = myMatchObj.filter((match)=>{
         return match.season.includes(2016);
         })
         .map((matchId)=>{
            return matchId.id;
      });
    //  console.log(season2016Id)

       csv()
      .fromFile(csvFilePathDeliveries)
      .then((myDeliveryObj)=>{
         if(myDeliveryObj == undefined)
         {
            return {};
         }
    //  console.log(myDeliveryObj[0])
           const extraRuns = myDeliveryObj.reduce((match, runs)=>{

            if(season2016Id.includes(runs.match_id))
            {

              if(match[runs.bowling_team])
              {
                 match[runs.bowling_team]+= Number(runs.extra_runs);
              }else
              {
                  match[runs.bowling_team] = Number(runs.extra_runs);
              }
            }

             return match;
        }, {});  

        if(extraRuns === undefined)
        {
           res.next({
               message : 'Internal Server Error',
               status : 500
          })
        }else
        {
           res.status(200).json(extraRuns);
        }

      //fs.writeFileSync(jsonFileOutputPath,JSON.stringify(extraRuns,null,2));
     });   
   });
})

module.exports = router;
