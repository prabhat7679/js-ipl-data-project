//   Find the highest number of times one player has been dismissed by another player
const express = require('express')
const router = express.Router();

const path=require("path");
const jsonFileOutputPath=path.resolve(__dirname,'../public/output/8-height-no-of-times-one-player-has-been-dismissed-by-other-player.json');
const csvFilePathDeliveries=path.resolve(__dirname,'../data/deliveries.csv');
const csvFilePathMatches=path.resolve(__dirname,'../data/matches.csv');
const csv= require('csvtojson');
const fs = require('fs');

router.get('/api/dismissed/mostTime',(req, res, next)=>{

 csv()
.fromFile(csvFilePathDeliveries)
.then((deliveriesObj)=>{
  
   if(deliveriesObj==undefined)
   {
    return {};
   }

    const dismissed = deliveriesObj.reduce((acc , curr)=>{
      
      if(curr.player_dismissed != '')
      {
         if(curr.dismissal_kind == 'run out' || curr.dismissal_kind == 'hit wicket'||curr.dismissal_kind=='retired hurt')
          {
             
          }else
          {
            if(acc[curr.batsman])
            {
                if(acc[curr.batsman][curr.bowler])
                {
                    acc[curr.batsman][curr.bowler]+=1;
                }else
                {
                    acc[curr.batsman][curr.bowler]=1;                      
                }
            }else
            {
                acc[curr.batsman]={};
                  
               acc[curr.batsman][curr.bowler]=1;
            }                
          }  
      }

      return acc;
    },{})


    let playerOutMostByAnotherSinglePlayer = Object.entries(dismissed)
    .map(bowler=>{
        let batsman=Object.entries(bowler[1]).sort((player1, player2)=>{
           return player2[1] - player1[1];
        })
        return [bowler[0],batsman[0]];
    }).sort((player3, player4) =>{

        return player4[1][1] - player3[1][1];
    });

    playerOutMostByAnotherSinglePlayer = playerOutMostByAnotherSinglePlayer[0];


        if(playerOutMostByAnotherSinglePlayer === undefined)
        {
            res.next({
            message : 'Internal Server Error',
            status : 500
        })
        }else
        {
            res.status(200).json(playerOutMostByAnotherSinglePlayer);
        }
    
         // fs.writeFileSync(jsonFileOutputPath,JSON.stringify(playerOutMostByAnotherSinglePlayer,null,4)); 
     });
})

module.exports = router;
