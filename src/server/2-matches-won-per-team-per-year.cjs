//  Number of matches won per team per year in IPL.
const express = require('express')
const router = express.Router()

const path= require("path");
const csvFilePath = path.join(__dirname,"../data/matches.csv");
//const jsonFileOutputPath= path.join(__dirname,"../public/output/2-matches-won-per-team-per-year.json");
const csv = require('csvtojson');
const fs= require("fs");

router.get('/api/matchesWon',(req, res, next)=>{

  csv()
  .fromFile(csvFilePath)
  .then((myMatchObj)=>{
        
      if(myMatchObj== undefined){
          return {};
      }
  
      const noOfMatchWonPerTeam= myMatchObj.reduce((year, Team)=>{
  
          if(year[Team.season]=== undefined)
          {
              year[Team.season]= {};
          }
  
            if(year[Team.season][Team.winner])
            {
                year[Team.season][Team.winner]+=1;
            }else
            {
                year[Team.season][Team.winner] =1;
            }
  
            return year;
      },{});
      

      if(noOfMatchWonPerTeam === undefined)
      {
        res.next({
              message : 'Internal Server Error',
              status : 500
        } )
      }else
      {
        res.status(200).json(noOfMatchWonPerTeam)
      }
       
      // fs.writeFileSync(jsonFileOutputPath, JSON.stringify(noOfMatchWonPerTeam,null,2), (err)=>{
      //   if(err){
      //     console.error(err);
      //   }
      // });
  });

})
module.exports= router;

