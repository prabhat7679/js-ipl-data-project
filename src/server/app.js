const express = require('express')
const path = require('path')
const port = require('./config')
const fs = require('fs')
const requestId = require('express-request-id')
const app = express();
app.use(requestId())

 const matchPerYear = require('./1-matches-per-year.cjs')
const matchWon = require('./2-matches-won-per-team-per-year.cjs')
const extraRunsIn2016 = require('./3-Extra-runs-conceded-per-team-in-the-year-2016.cjs')
const top10Bowler = require('./4-top-10-economical-bowlers-in-the-year-2015.cjs')
const teamWonTossMatch = require('./5-each-team-won-the-toss-and-won-the-match.cjs')
const palyerOfMatch = require('./6-highest-no-of-player-of-the-match-each-season.cjs')
const strikeRate = require('./7-strike-rate-of-a-batsman-each-season.cjs')
const dismissed = require('./8-height-no-of-times-one-player-has-been-dismissed-by-other-player.cjs')
const bestBowlerInSuperOver = require('./9-bowler-with-best-economy-in-super-overs.cjs')

//  middleware function 
const requestLogger = (req, res, next) => {

  if(req.originalUrl ==='/favicon.ico')
  {
    next();
  }else
 {
    const loggingData =` Request ID: ${req.id} - Method : ${req.method} - Url : ${req.originalUrl}\n`;
      fs.appendFile('request.log', loggingData, (err) => {
        if (err) 
        {
          console.error('Error writing to request log:', err);
        }
    });
    next();
  }
};

app.use(requestLogger);

app.get('/api/matches-per-year',matchPerYear)

app.get('/api/matchesWon',matchWon)

app.get('/api/extraRun/2016',extraRunsIn2016)

app.get('/api/Economical/Bowler/2015',top10Bowler)

app.get('/api/teamWonToss/wonMatch',teamWonTossMatch)

app.get('/api/palyerOfMatch/season',palyerOfMatch)

app.get('/api/strikeRate/batsman',strikeRate)

app.get('/api/dismissed/mostTime',dismissed)

app.get('/api/bowlerEconomy/superOver',bestBowlerInSuperOver)

app.get('/logs', (req, res) => {
    fs.readFile('request.log', 'utf8', (err, data) => {
      if (err)
       {
          console.error('Error reading request log:', err);
          res.status(500).send({"Error" : "Internal Server Error"});
      } else 
      {
         res.send(data);
      }
    });
  });


app.use(express.static(path.join(__dirname,'../public')))

app.use((req, res)=>{
    res.status(404).send(`
        <h1>Invalid url! please enter valid url<h1> 
        <h2>  valid urls are </h2>
        <h3> /api/matches-per-year</h3>  
        <h3>/api/matchesWon</h3> 
        <h3>/api/extraRun/2016</h3>
        <h3>/api/economical/bowler/2015</h3>
        <h3>/api/teamWonToss/wonMatch</h3>
        <h3>/api/palyerOfMatch/season</h3>
        <h3>/api/strikeRate/batsman</h3>
        <h3>/api/dismissed/mostTime</h3>
        <h3>/api/bowlerEconomy/superOver</h3>
    `)
})

app.get((err, req, res, next)=>{

    console.error(err);

    res.status(err.status.json({error : err.message}))
})

app.listen(port,()=>{
    console.log(`port ${port} is running`)
})