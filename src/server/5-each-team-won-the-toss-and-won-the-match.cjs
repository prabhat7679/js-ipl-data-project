//    Find the number of times each team won the toss and also won the match
const express = require('express')

const router = express.Router();
const path=require("path");
const jsonFileOutputPath=path.resolve(__dirname,'../public/output/5-each-team-won-the-toss-and-won-the-match.json');
//const csvFilePathDeliveries=path.resolve(__dirname,'../data/deliveries.csv');
const csvFilePathMatches=path.resolve(__dirname,'../data/matches.csv');
const csv= require('csvtojson');
const fs = require('fs');

router.get('/api/teamWonToss/wonMatch',(req,res, next)=>{

 csv()
.fromFile(csvFilePathMatches)
.then((myMatchObj)=>{
    //  console.log(myMatchObj[0]);
   if(myMatchObj==undefined)
   {
    return {};
   }

    const teamWonTossAndMatch=myMatchObj.reduce((match , current)=>{
      if(match[current.toss_winner])
      {
        if(current.toss_winner === current.winner)
        {
            match[current.toss_winner]+=1;
        }
    }else
    {
        if(current.toss_winner === current.winner)
        {
            match[current.toss_winner]=1;
        }
    }
        return match;

    },{})

    if(teamWonTossAndMatch === undefined)
    {
       res.next({
       message : 'Internal Server Error',
       status : 500
    })
    }else
    {
        res.status(200).json(teamWonTossAndMatch);
    }
   
    //fs.writeFileSync(jsonFileOutputPath,JSON.stringify(teamWonTossAndMatch,null,2));
  });
})

module.exports = router;
