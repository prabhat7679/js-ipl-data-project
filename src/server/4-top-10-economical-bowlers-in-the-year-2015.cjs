//   Top 10 economical bowlers in the year 2015

const express = require('express')
const router = express.Router();

const path=require("path");
const jsonFileOutputPath=path.resolve(__dirname,'../public/output/4-top-10-economical-bowlers-in-the-year-2015.json');
const csvFilePathDeliveries=path.resolve(__dirname,'../data/deliveries.csv');
const csvFilePathMatches=path.resolve(__dirname,'../data/matches.csv');
const csv= require('csvtojson');
const fs = require('fs');
router.get('/api/Economical/Bowler/2015',(req, res, next)=>{
   
 csv()
.fromFile(csvFilePathMatches)
.then((myMatchObj)=>{
  //  console.log(myMatchObj[0]);
   if(myMatchObj==undefined){
    return {};
   }
   // find the season id of 2015
    const seasonIdOf2015 = myMatchObj.filter((year)=>{
        return year.season.includes('2015')
    }).map((matchId)=>{
        return matchId.id;
    })

      csv()
      .fromFile(csvFilePathDeliveries)
      .then((myDeliveryObj)=>{

      //  console.log(myDeliveryObj[0])
         
        const economicalBowlerIn2015 = myDeliveryObj.reduce(( acc, currentData)=>{
             
            if(seasonIdOf2015.includes(currentData.match_id)){
                if(acc[currentData.bowler])
                {
                    
                    if(acc[currentData.bowler].bye_runs || acc[currentData.bowler].legbye_runs)
                    {
                        acc[currentData.bowler].runs+=Number(0);
                    }else
                    {
                        acc[currentData.bowler].runs+=Number(currentData.total_runs);
                    }
                      // balls

                    if((acc[currentData.bowler].wide_runs || acc[currentData.bowler].noball_runs))
                    {
                      acc[currentData.bowler].balls+=Number(0);
                    }else
                    {
                      acc[currentData.bowler].balls+=Number(1)
                    }

                }else
                {
                    acc[currentData.bowler] = {};
    
                    acc[currentData.bowler].runs = Number(currentData.total_runs);

                    if((acc[currentData.bowler].wide_runs || acc[currentData.bowler].noball_runs))
                    {
                        acc[currentData.bowler].balls+= Number(0);
                    }else
                    {
                        acc[currentData.bowler].balls = Number(1)
                    }

                }

            }
            
             return acc;
        },{})

       // console.log(economicalBowlerIn2015)

        const allBowlerEconomy = Object.entries(economicalBowlerIn2015).map((data)=>{
               const name = data[0];
               let run = data[1].runs;
               let ball = data[1].balls;
               let economy = (run/ball)*6;
               return [name,economy];
        })
             //     console.log(typeof allBowlerEconomy)

               allBowlerEconomy.sort((a, b)=>{
               return a[1]-b[1];
               })

           //    console.log(allBowlerEconomy)

               const top10EconomicalBowler = allBowlerEconomy.slice(0,10);
               
            //     console.log(top10EconomicalBowler)

            if(top10EconomicalBowler === undefined)
            {
               res.next({
               message : 'Internal Server Error',
               status : 500
            })
            }else
            {
                res.status(200).json(top10EconomicalBowler);
            }

            // fs.writeFileSync(jsonFileOutputPath, JSON.stringify(top10EconomicalBowler,null,2), (err)=>{
            //     if(err)
            //     {
            //         console.log(err);
            //     }
            //  });
        })
    });   

})
 module.exports = router;
