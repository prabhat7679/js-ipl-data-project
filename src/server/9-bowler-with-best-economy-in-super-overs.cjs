const express = require('express')
const router = express.Router()

const path=require("path");
const jsonFileOutputPath=path.resolve(__dirname,'../public/output/9-bowler-with-best-economy-in-super-overs.json');
const csvFilePathDeliveries=path.resolve(__dirname,'../data/deliveries.csv');
const csvFilePathMatches=path.resolve(__dirname,'../data/matches.csv');
const csv= require('csvtojson');
const fs = require('fs');


router.get('/api/bowlerEconomy/superOver',(req, res, next)=>{

    csv()
    .fromFile(csvFilePathDeliveries)
    .then((deliveriesObj)=>{
       
       if(deliveriesObj==undefined){
        return {};
       }
    
       const bestEconomyObj = deliveriesObj.filter((ball)=>{
        return ball.is_super_over !== '0';
       })
       .reduce((superOverBowler, delivery)=>{
        if(superOverBowler[delivery.bowler])
        {
            superOverBowler[delivery.bowler].runs+=Number(delivery.total_runs);
            superOverBowler[delivery.bowler].ball +=1;
            superOverBowler[delivery.bowler].economyValue = ((superOverBowler[delivery.bowler].runs/superOverBowler[delivery.bowler].ball)*6);
        }else
        {
            superOverBowler[delivery.bowler]= {};
            superOverBowler[delivery.bowler].runs = Number(delivery.total_runs);
            superOverBowler[delivery.bowler].ball =1;
        }
    
        return superOverBowler;
     },{});
    
        const bowlerEconomy = Object.entries(bestEconomyObj).reduce((bowlerName, bowlerEconomyValue)=>{
    
            bowlerName[bowlerEconomyValue[0]] = bowlerEconomyValue[1].economyValue;
    
            return bowlerName;
        },{});
    
        const bestBowler = Object.fromEntries(Object.entries(bowlerEconomy)
        .sort((player1, player2)=>{
            return player1[1]- player2[1];
        })
        .slice(0, 1));
    
        if(bestBowler === undefined)
        {
            res.next({
            message : 'Internal Server Error',
            status : 500
        })
        }else
        {
            res.status(200).json(bestBowler);
        }

   // fs.writeFileSync(jsonFileOutputPath,JSON.stringify(bestBowler,null,2));
    });

})
module.exports = router;

    
