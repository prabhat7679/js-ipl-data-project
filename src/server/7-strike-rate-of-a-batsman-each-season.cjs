const express = require('express')
const router = express.Router()
const fs = require("fs");
const path = require("path");
const csvFilePathMatches = path.resolve(__dirname, "../data/matches.csv");
const csvFilePathDeliveries = path.resolve(__dirname, "../data/deliveries.csv");
const jsonFilePath = "../public/output/7-strike-rate-of-a-batsman-each-season.json";
const csv = require("csvtojson");

router.get('/api/strikeRate/batsman',(req,res,next)=>{

  csv()
  .fromFile(csvFilePathMatches)
  .then((matchesObj) => {
    if (matchesObj === undefined) {
      return {};
    }

    const seasonId = matchesObj.reduce((matchId, match) => {
      matchId[match.id] = match.season;

      return matchId;
    }, {});

    csv()
      .fromFile(csvFilePathDeliveries)
      .then((deliveries) => {
        if (deliveries === undefined) {
          return {};
        }

        const playerData = deliveries.reduce((batsmanData, delivery) => {
          let season = seasonId[delivery.match_id];

          if (batsmanData[delivery.batsman]) {
            if (batsmanData[delivery.batsman][season]) {
              batsmanData[delivery.batsman][season].totalRuns += Number(
                delivery.total_runs
              );
              batsmanData[delivery.batsman][season].totalBallsFaced += 1;
              batsmanData[delivery.batsman][season].strikeRate = Number(
                (batsmanData[delivery.batsman][season].totalRuns /
                  batsmanData[delivery.batsman][season].totalBallsFaced) *
                  100
              ).toFixed(2);
            } else {
              batsmanData[delivery.batsman][season] = {};
              batsmanData[delivery.batsman][season].totalRuns = Number(
                delivery.total_runs
              );
              batsmanData[delivery.batsman][season].totalBallsFaced = 1;
            }
          } else {

            batsmanData[delivery.batsman] = {};
            batsmanData[delivery.batsman][season] = {};
            batsmanData[delivery.batsman][season].totalRuns = Number(
              delivery.total_runs
            );
            batsmanData[delivery.batsman][season].totalBallsFaced = 1;
          }

          return batsmanData;
        }, {});

        const strikeRateOfAllBatsman = Object.entries(playerData).reduce(
          (batsman, batsmanStrikeDetail) => {
            batsman[batsmanStrikeDetail[0]] = Object.entries(
              batsmanStrikeDetail[1]
            ).reduce((year, strikeValue) => {
              year[strikeValue[0]] = strikeValue[1].strikeRate;

              return year;
            }, {});

            return batsman;
          }, {});

            if(strikeRateOfAllBatsman === undefined)
            {
               res.next({
               message : 'Internal Server Error',
               status : 500
            })
            }else
            {
                res.status(200).json(strikeRateOfAllBatsman);
            }

       // fs.writeFileSync(jsonFilePath, JSON.stringify(strikeRateOfAllBatsman,null,2));
      });
  });

})

module.exports= router;

